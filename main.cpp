#include <iostream>

using namespace std;
const int tamanho=5;

struct candidato{
    char nome[60];
    int tempo;
};

struct candidato candidatos[tamanho];//Posso chamar a estrutura apenas pelo nome -> candidato candidatos [tamanho].

void lerEstrutura(struct candidato cand[], int tamanho){
    for(int i=0; i<tamanho; i++){
        cout<< "Nomes: ";
        cin.getline(cand[i].nome,60);//para vetores de strings que contenha espacos.
        cout<<"Tempo:";
        cin>> cand[i].tempo;// insere o valor em tempo da estrutura e ignora o parametro nome.
        cout<<endl;
        cin.ignore(); // limpa a area de leitura para n�o acumular caracteres nessa area
    }
}

void imprimirEstrutura(struct candidato cand[], int tamanho){
    for (int i=0; i<tamanho; i++){
        cout<<"Nome: " << cand[i].nome<<endl;
        cout<<"Tempo: "<< cand[i].tempo<<endl;
        cout<<endl;
    }
}

int main()
{
    lerEstrutura(candidatos, tamanho);
    imprimirEstrutura(candidatos, tamanho);
    return 0;
}
